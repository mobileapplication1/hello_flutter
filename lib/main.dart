import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";

String thaiGreeting = "สวัสดี ฟลัตเตอร์";
String chineseGreeting = "Nihao Flutter";

String laosGreeting = "sabaidee flutter";
String japaneseGreeting = "konijiwa flutter";
class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
    @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != englishGreeting?
                    englishGreeting : spanishGreeting;
                  });
                },
                icon: Icon(Icons.language)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != thaiGreeting?
                    thaiGreeting : chineseGreeting;
                  });
                },
                icon: Icon(Icons.sort_by_alpha)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != laosGreeting?
                    laosGreeting : japaneseGreeting;
                  });
                },
                icon: Icon(Icons.translate))
          ],
        ),
        body: Center(
          child: Text(displayText,
          style: TextStyle(fontSize: 24)
          ),
        ),
      ),
    );
  }
}
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text("Hello Flutter!!",
//           style: TextStyle(fontSize: 24)
//           ),
//         ),
//       ),
//     );
//   }
// }